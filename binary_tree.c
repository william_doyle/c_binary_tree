#include <stdlib.h>
#include <stdio.h>
#include "binary_tree.h"


/*	FILENAME	binary_tree.c
 *	PURPOSE		Implement binary tree in C while learning/demonstrating OOP techniques in C
 *	DATE		DECEMBER 15th 2019
 *	
 *
 */





/*	method name:	make_node()
 *	args:		1 -- data value
 *	returns:	a ptr to a new node
 * */
Node * make_node(_TYPE_ prov_val){
	Node * return_val = (Node *) malloc(sizeof(Node));
	return_val->data = prov_val;
	return return_val;
}

/*	method name:	BT_CLEAR()
 *	args:		1 -- Node * to place to start clear recursive call
 *	returns:	void
 * */
void BT_CLEAR(Node * start){
	if (start->left != NULL){
		BT_CLEAR(start->left);
	}
	if (start->right != NULL){
		BT_CLEAR(start->right);
	}
	free(start);
}

/*	method name: 	BT_PRINT()
 *	args:		node ptr start pos
 *	returns:	void
 * */
void BT_PRINT(Node * start){
	if (start == NULL) {
		return;
	}
	BT_PRINT(start->left);
	printf("%d ", start->data);
	BT_PRINT(start->right);
}

void BT_INSERT(Node ** start, _TYPE_ provval){
	/* add value to tree using make_node() function */
	if (*start == NULL){
		*start = make_node(provval);
	//	return;
	}
	if (provval < (*start)->data){
		BT_INSERT(&(*start)->left, provval);
	}
	else if (provval > (*start)->data){
		BT_INSERT(&(*start)->right, provval);
	}			
}
/*	who, what, when, where, why, How?
 *	@author William Doyle
 *	@method_name SWITCH_ORDER takes ptr to binary tree, and a function ptr to a new print function which itself takes an argument of one node
 *	@date Dec 22nd 2019
 *	@location binary_tree/binary_tree.c
 *	@purpose allow easy swaping of binary_tree print function
 **/
void SWITCH_ORDER(BinaryTree * bt , void (*new_print_method)(Node *)){
	bt->print = new_print_method;
}

/* set binary tree function pointers to correct values */
void prep_binary_tree(BinaryTree * _btree, _TYPE_ ROOT_DATA_VALUE ){
	_btree->print = BT_PRINT;
	_btree->clear = BT_CLEAR;
	_btree->insert = BT_INSERT;

	/* DEFINE VALUES */
	_btree->root = make_node(ROOT_DATA_VALUE);

}









