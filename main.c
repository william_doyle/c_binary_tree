#include "binary_tree.h"



/*	NOTES:
 *		Compile with binary_tree.c
 *
 * */

#define ver_num 1//change to check if compilation happened

int main(){
	printf("Version Number %d\n", ver_num);		//make sure recompilation happened
	BinaryTree bt;
	prep_binary_tree(&bt, 100);
	
	int arr[] = {3, 4, 567};
	
	for (int i = 0; i < sizeof(arr)/sizeof(int); i++){
		bt.insert(&bt.root, arr[i]);
	}



	bt.print(bt.root);
	puts("");


	bt.clear(bt.root);

};
