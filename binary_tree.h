#include <stdlib.h>
#include <stdio.h>

#define _TYPE_ int	/* Inorder to make tree work with other types you must redefine _TYPE_ and edit printf lines where %d can be found */


/*	struct: 	node
 *	description: 	a node in a binary tree holds a value of data... a ptr to left node and a ptr to right node
 *	date: 		Dec 15th 2019
 *	author: 	William Doyle
 * */
typedef struct node{
	_TYPE_ data;
	struct node * left;	// Qusetion: can I replace struct node with Node inside this struct definition?
	struct node * right;	// Apparently not... I think this is because a top down compiler sees typedef struct node but has yet to see "Node" yet
} Node;

struct node * make_node(_TYPE_ prov_val);

void BT_CLEAR(struct node * start);


void BT_PRINT(struct node * start);


void BT_INSERT(struct node ** start, _TYPE_ provval);

/*	binary_tree
 *	Description:	wrapper for binary tree nodes... has some OOP features
 *	Date:		December 15th 2019
 *	Author:		William Doyle	
 * */
typedef struct binary_tree{
	char name[25];
	char info[50];
	struct node * root;			//root
	void (*print)(struct node *);		//prints binary tree
	void (*clear)(struct node *);		//erase contents of binary tree
	void (*insert)(struct node **, _TYPE_);	//insert value of _TYPE_ into binary tree
} BinaryTree;

/* set binary tree function pointers to correct values */
void prep_binary_tree(struct binary_tree * _btree, _TYPE_ ROOT_DATA_VALUE );

void SWITCH_ORDER(BinaryTree *, void(*new_print_method)(Node *) );//like this --notice we have a function that accepts a pointer to a function as an argument...COOL!
